

void airconditionerOff() {
  digitalWrite(PIN_LED_COLD, LOW);
  digitalWrite(PIN_LED_HOT, LOW);
}

void airconditionerHeat() {
  digitalWrite(PIN_LED_COLD, HIGH);
  digitalWrite(PIN_LED_HOT, LOW);
}

void airconditionerCool() {
  digitalWrite(PIN_LED_COLD, LOW);
  digitalWrite(PIN_LED_HOT, HIGH);
}

void airconditionerAuto(byte * temperature1, byte * temperature2) {
  if(*temperature1 != NO_TEMP && *temperature2 != NO_TEMP)
  {  
    signed int difference = (int)*temperature2 -  (int)*temperature1;
    printDifference(&difference);

    if(difference >= 2) 
    {
      airconditionerCool();
      client.publish(mqtt_airconTopic, "automatic cooling");
    } else if (difference <= -2)
    {
      airconditionerHeat();
      client.publish(mqtt_airconTopic, "automatic heating");
    } else {
      airconditionerOff();
      client.publish(mqtt_airconTopic, "automatic off");
    }
  }
}

