void printTemp(const String name, const byte * temperature, const byte * humidity, const byte * data)
{
  Serial.print(name);
  Serial.print(" -->");
  Serial.print((int)*temperature); 
  Serial.print(" *C, ");
  Serial.print((int)*humidity); 
  Serial.println(" %");
}

void printDifference(const signed int * difference) {
  Serial.print("Difference is ");
  Serial.print(*difference);
  Serial.println(" *C");
}


byte readDHTSensor(SimpleDHT11 * sensor, byte pin) {
  byte temperature = 0;
  byte humidity = 0;
  byte data[40] = {0};
  
  if (!sensor->read(pin, &temperature, &humidity, data)) {
    printTemp("Sensor", &temperature, &humidity, data);
    return temperature;
  } else {
    Serial.println("Read DHTSensor failed");
  }
  return NO_TEMP;
}

