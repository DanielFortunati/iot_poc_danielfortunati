
boolean isCardPresent() {
    return mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial();
}

void printCard(String cardID) {
  Serial.print("Card UID:");
  Serial.println(cardID);
}

String readCard() {
  int arrayCount = 0;
  char cardID[9] = {'\0'};
  
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    String hexString = String(mfrc522.uid.uidByte[i], HEX);
    cardID[arrayCount++] = hexString[0];
    cardID[arrayCount++] = hexString[1];
  }
  cardID[arrayCount++] = '\0';
  
  printCard(String(cardID));
  
  return String(cardID);
}

