#include <SimpleDHT.h>
#include <SPI.h>
#include <MFRC522.h>
#include <ESP8266WiFi.h>

#include <PubSubClient.h>

#define ssid "DanielWLAN"
#define pass "123dolly321"
#define mqtt_server "192.168.137.180" // [IP ADDRESS OF RASPBERRY PI]
#define mqtt_port 1883

#define mqtt_callbackTopic "NODERED"
#define mqtt_tempTopic1 "growingArea/temp"
#define mqtt_tempTopic2 "equipmentRoom/temp"
#define mqtt_cardTopic "equipmentRoom/card"
#define mqtt_airconTopic "equipmentRoom/aircon"

WiFiClient espClient;
PubSubClient client(espClient);

#define NO_TEMP 255
#define PIN_TEMP1 3 
#define PIN_TEMP2 16 //D2

#define PIN_LED_COLD 15
#define PIN_LED_HOT 4
/**
 * SDA D8
 * SCK D4 / D13
 * MOSI D7 / D11
 * MISO D6 / D12
 * RST D3 / D15
 */
#define PIN_RFID_RST   5
#define PIN_RFID_SS    0

static MFRC522 mfrc522(PIN_RFID_SS, PIN_RFID_RST);

#define AC_MODE_ON_HEAT "ACONH"
#define AC_MODE_ON_COOL "ACONC"
#define AC_MODE_OFF "ACOF"
#define AC_MODE_AUTO "ACAU"

typedef enum {AC_AUTO, AC_ON_HEAT, AC_ON_COOL, AC_OFF} AIRCON_STATUS;

static AIRCON_STATUS currentAcStatus = AC_AUTO;

static SimpleDHT11 DHTSensor1; // light 
static SimpleDHT11 DHTSensor2;

static unsigned int countdownTemp = 0;
static unsigned int countdownRFID = 0;

void setup() 
{
  pinMode(PIN_LED_COLD, OUTPUT);
  digitalWrite(PIN_LED_COLD, LOW);
  
  pinMode(PIN_LED_HOT, OUTPUT);
  digitalWrite(PIN_LED_HOT, LOW);

  Serial.begin(9600);
  while (!Serial);
  SPI.begin();    
  
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  
  mfrc522.PCD_Init();  // Init MFRC522 card
}

void loop() 
{
  if (!client.connected()) 
  {
    reconnect();
  }
  
  client.loop();

  if(countdownRFID == 0)  
  {
    if(isCardPresent()) 
    {
      String cardID = readCard();
      char charArrayBuffer[9];
      cardID.toCharArray(charArrayBuffer, 9);
      client.publish(mqtt_cardTopic, charArrayBuffer);
      countdownRFID = 20;
    }
    
  } else {
    countdownRFID--;
  }
  
  if(countdownTemp == 0) 
  {
    countdownTemp = 20;
    Serial.println("=================================");
    
    byte temperature1 = readDHTSensor(&DHTSensor1, PIN_TEMP1);
    client.publish(mqtt_tempTopic1, &temperature1, true);
    
    byte temperature2 = readDHTSensor(&DHTSensor2, PIN_TEMP2);
    client.publish(mqtt_tempTopic2, &temperature2, true);
    
    switch(currentAcStatus) {
      case AC_AUTO: airconditionerAuto(&temperature1, &temperature2); break;
      
      case AC_ON_HEAT: airconditionerHeat(); break;
      
      case AC_ON_COOL: airconditionerCool(); break;
      
      case AC_OFF: airconditionerOff(); break;
    }
  } else {
    countdownTemp--;
  }
  
  delay(50);
}










