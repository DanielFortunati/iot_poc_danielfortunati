
void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      client.setCallback(callBack);
      client.subscribe(mqtt_callbackTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void callBack(char* topic, byte* payload, unsigned int length) {
  Serial.println("NodeRED message (topic \"");
  Serial.print(*topic);
  Serial.print(": ");
  
  String message = "";
  for(int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  
  Serial.print(message);
  Serial.print("\")");
  Serial.println();
  
  if (message.equals(AC_MODE_AUTO)){
    currentAcStatus = AC_AUTO;
  } else if(message.equals(AC_MODE_OFF)) {
    currentAcStatus = AC_OFF;
    client.publish(mqtt_airconTopic, "manual off");
  } else if(message.equals(AC_MODE_ON_HEAT)) {
    currentAcStatus = AC_ON_HEAT;
    client.publish(mqtt_airconTopic, "manual heating");
  } else if(message.equals(AC_MODE_ON_COOL)) {
    currentAcStatus = AC_ON_COOL;
    client.publish(mqtt_airconTopic, "manual cooling");
  } else {
    currentAcStatus = AC_AUTO;
    client.publish(mqtt_airconTopic, "failed");
  }
  
}
